# Basic Assembly Language Emulator

This project's purpose is to provide students learning the System/360 Basic Assembly Language (BAL) a tool to quickly run their programs and display helpful errors and warnings.

# Notes

I have added a bash script you can run by entering "./buildrun" into your terminal. This script compiles both the assembler and emulator, then the script runs the assembler with the provided "input.asm", and finally, it emulates the resulting "obj" object code file.

# Screenshot

![Sreenshot](screenshot.png)