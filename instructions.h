#include <functional>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

namespace BAL {

    /* Operation class */
    class Operation {
    public:
        // Constructor
        Operation(uint8_t bytecode, std::string mnemonic, std::string type);

        // Operation's function for emulator
        std::function<void()> func;

        // Operation data
        uint8_t opcode;
        std::string mnemonic;
        enum Type { I, RR, RS, RX, SI, SS, UNK } type;
    };

    /* Instructions class */
    class Instructions {
    public:
        // Attempt to load instruction mnemonics dictionary
        Instructions();//const char* filename);

        // Grab the requested operation by mnemonic
        const Operation* GetOperationByName(std::string name);
        
        //void SetOperationFuncByName(void* func);

    private:        
        // Operations array mapped to actual opcode
        Operation* operations[0xFF];
        
        // Mnemonics dictionary
        std::map<std::string, Operation*> mnemonics;
    };



    /* Class Implementation */
    Operation::Operation(uint8_t opcode, std::string mnemonic, std::string type) {
        this->opcode = opcode;
        
        this->mnemonic = mnemonic;

        if (type == "I") this->type = I;
        else if (type == "RR") this->type = RR;
        else if (type == "RS") this->type = RS;
        else if (type == "RX") this->type = RX;
        else if (type == "SI") this->type = SI;
        else if (type == "SS") this->type = SS;
        else this->type = UNK;
    }

    Instructions::Instructions() {//const char *filename) {
        const char* filename = "360-instructions.csv";
        std::ifstream infile(filename);
        if (!infile) std::cout << "ERROR! Initializing instructions dictionary has failed. System/360 instructions could not be found in" << filename << '\n';
            
        std::string line;
        while (std::getline(infile, line)) {
            std::stringstream ss(line);
            std::string bytecode, name, type;
            ss >> std::hex >> bytecode;
            ss >> name;
            ss >> type;
            
            uint8_t opcode = strtoul(bytecode.c_str(), nullptr, 16);
            operations[opcode] = new Operation(opcode, name, type);
            mnemonics.insert(std::pair<std::string, Operation*>(name, operations[opcode]));
        }
    }

    const Operation* Instructions::GetOperationByName(std::string name) {
        auto it = mnemonics.find(name);
        if (it != mnemonics.end()) {
            return it->second;
        } else {
            return nullptr;
        }
    }

}

