#include <iostream>

#include "assembler.h"

int main(int argc, char *argv[]) {

    // Ensure proper argument count
    if (argc < 3) {
        std::cout << "Not enough arguments\n";
        return -1;
    }
    
    // Load input source into assembler
    Assembler assembler;
    if (!assembler.LoadSourceFromFile(argv[1])) {
        std::cout << "Error opening input file\n";
        return -1;
    }
    
    // Assemble object code to file
    if (!assembler.Assemble(argv[2])) {
        std::cout << "Errors during assembly\n";
        return -1;
    }

    return 0;
}
