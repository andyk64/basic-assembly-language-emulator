#pragma once

#include <iostream>
#include <iomanip>

#include <algorithm>

#include <fstream>
#include <cstring>
#include <vector>
#include <map>

#include <sstream>

#include "instructions.h"

class Assembler {

public:

    bool LoadSourceFromFile(const char *filename) {
        // Try to open file
        std::ifstream infile;
        infile.open(filename, std::ios_base::binary);
        if (!infile) return false;
        
        // Load lines into member
        std::string line;
        while (std::getline(infile, line)) {
            // Ignore unnecessary lines (comments, blank lines, etc)
            if (line[0] != '/' && line[0] != '*' && line[0] != '\0') {
                lines.push_back(line);
            }

            if (line[0] == '\0') {
                std::cout << "WARNING: Empty line at line " << lines.size() << '\n';
            }
        }

        // Close file
        infile.close();
        return true;
    }
    
    bool Assemble(const char *filename) {
        std::vector<uint8_t> bytecode;
        uint32_t bytepos = 0;
        
        struct LabelInstance {
            std::string label;
            uint32_t pos;
        };
        std::vector<LabelInstance> labelsToResolve;
    
        for (size_t i = 1; i <= lines.size(); i++) {
            std::string label, mnemonic, params;            
            ParseLine(i, label, mnemonic, params);

            if (label != "") {
                labels.insert(std::pair<std::string, uint32_t>(label, bytecode.size()));
            }
            
            const BAL::Operation* op = instructions.GetOperationByName(mnemonic);
            if (op != nullptr) {
            
                std::string params2 = params;
                std::replace_if(params2.begin(), params2.end(), ::ispunct, ' ');
                std::stringstream ss(params2);
            
                if (op->type == BAL::Operation::I) {
                    
                    uint8_t bytes[2] = { op->opcode, 0 };
                    uint16_t temp = 0;
                    
                    // I
                    ss >> temp;
                    bytes[1] += temp;
                    
                    bytecode.push_back(bytes[0]);
                    bytecode.push_back(bytes[1]);
                    
                } else if (op->type == BAL::Operation::RR) {
                
                    uint8_t bytes[2] = { op->opcode, 0 };
                    uint16_t temp = 0;
                    
                    // R1
                    if (op->opcode != 0x07) {
                        ss >> temp;
                        bytes[1] += temp << 4;
                    } else {
                        // R1 exceptions (for built in branch masks)
                        if (op->mnemonic == "BCR") {
                            ss >> temp;
                            bytes[1] += temp << 4;
                        }
                        else if (op->mnemonic == "BR") bytes[1] += 0xF0;
                        else if (op->mnemonic == "NOPR") bytes[1] += 0x00;
                        else if (op->mnemonic == "BOR") bytes[1] += 0x10;
                        else if (op->mnemonic == "BHR") bytes[1] += 0x20;
                        else if (op->mnemonic == "BPR") bytes[1] += 0x20;
                        else if (op->mnemonic == "BLR") bytes[1] += 0x40;
                        else if (op->mnemonic == "BMR") bytes[1] += 0x40;
                        else if (op->mnemonic == "BNER") bytes[1] += 0x70;
                        else if (op->mnemonic == "BNZR") bytes[1] += 0x70;
                        else if (op->mnemonic == "BER") bytes[1] += 0x80;
                        else if (op->mnemonic == "BZR") bytes[1] += 0x80;
                        else if (op->mnemonic == "BNLR") bytes[1] += 0xB0;
                        else if (op->mnemonic == "BNMR") bytes[1] += 0xB0;
                        else if (op->mnemonic == "BNHR") bytes[1] += 0xD0;
                        else if (op->mnemonic == "BNPR") bytes[1] += 0xD0;
                        else if (op->mnemonic == "BNOR") bytes[1] += 0xE0;
                    }
                    
                    // R2
                    ss >> temp;
                    bytes[1] += temp;
                    
                    bytecode.push_back(bytes[0]);
                    bytecode.push_back(bytes[1]);
                
                } else if (op->type == BAL::Operation::RS) {
                
                    uint8_t bytes[4] = { op->opcode, 0, 0, 0 };
                    uint16_t temp;
                    
                    ss >> temp;
                    bytes[1] += temp << 4;
                    
                    // D, X, B
                    if (ss.get() && isdigit(ss.peek())) {
                        // D
                        ss >> temp;
                        bytes[2] += temp >> 8;
                        bytes[3] += temp;
                        
                        // X
                        ss >> temp;
                        bytes[1] += temp;
                        
                        // B
                        ss >> temp;
                        bytes[2] += temp << 4;
                    } else {
                        LabelInstance resolveMe;
                        ss >> resolveMe.label;
                        resolveMe.pos = bytecode.size() + 1;
                        labelsToResolve.push_back(resolveMe);
                    }
                    
                    bytecode.push_back(bytes[0]);
                    bytecode.push_back(bytes[1]);
                    bytecode.push_back(bytes[2]);
                    bytecode.push_back(bytes[3]);
                    
                } else if (op->type == BAL::Operation::RX) {

                    uint8_t bytes[4] = { op->opcode, 0, 0, 0 };
                    uint16_t temp;
                    
                    // R1
                    if (op->opcode != 0x47) {
                        ss >> temp;
                        bytes[1] += temp << 4;
                    } else {
                        // R1 exceptions (for built in branch masks)
                        if (op->mnemonic == "BC") {
                            ss >> temp;
                            bytes[1] += temp << 4;
                        }
                        else if (op->mnemonic == "B") bytes[1] += 0xF0;
                        else if (op->mnemonic == "NOP") bytes[1] += 0x00;
                        else if (op->mnemonic == "BO") bytes[1] += 0x10;
                        else if (op->mnemonic == "BH") bytes[1] += 0x20;
                        else if (op->mnemonic == "BP") bytes[1] += 0x20;
                        else if (op->mnemonic == "BL") bytes[1] += 0x40;
                        else if (op->mnemonic == "BM") bytes[1] += 0x40;
                        else if (op->mnemonic == "BNE") bytes[1] += 0x70;
                        else if (op->mnemonic == "BNZ") bytes[1] += 0x70;
                        else if (op->mnemonic == "BE") bytes[1] += 0x80;
                        else if (op->mnemonic == "BZ") bytes[1] += 0x80;
                        else if (op->mnemonic == "BNL") bytes[1] += 0xB0;
                        else if (op->mnemonic == "BNM") bytes[1] += 0xB0;
                        else if (op->mnemonic == "BNH") bytes[1] += 0xD0;
                        else if (op->mnemonic == "BNP") bytes[1] += 0xD0;
                        else if (op->mnemonic == "BNO") bytes[1] += 0xE0;
                    }
                    
                    // D, X, B
                    if (ss.get() && isdigit(ss.peek())) {
                        // D
                        ss >> temp;
                        bytes[2] += temp >> 8;
                        bytes[3] += temp;
                        
                        // X
                        ss >> temp;
                        bytes[1] += temp;
                        
                        // B
                        ss >> temp;
                        bytes[2] += temp << 4;
                    } else {
                        LabelInstance resolveMe;
                        ss >> resolveMe.label;
                        resolveMe.pos = bytecode.size() + 1;
                        labelsToResolve.push_back(resolveMe);
                    }
                    
                    bytecode.push_back(bytes[0]);
                    bytecode.push_back(bytes[1]);
                    bytecode.push_back(bytes[2]);
                    bytecode.push_back(bytes[3]);
                
                } else if (op->type == BAL::Operation::SI) {
                
                    uint8_t bytes[4] = { op->opcode, 0, 0, 0 };
                    uint16_t temp;
                    
                    // I
                    ss >> temp;
                    bytes[1] += temp;
                    
                    // D, B
                    if (ss.get() && isdigit(ss.peek())) {
                        // D
                        ss >> temp;
                        bytes[2] += temp >> 8;
                        bytes[3] += temp;

                        // B
                        ss >> temp;
                        bytes[2] += temp << 4;
                    } else {
                        LabelInstance resolveMe;
                        ss >> resolveMe.label;
                        resolveMe.pos = bytecode.size() + 1;
                        labelsToResolve.push_back(resolveMe);
                    }
                    
                    bytecode.push_back(bytes[0]);
                    bytecode.push_back(bytes[1]);
                    bytecode.push_back(bytes[2]);
                    bytecode.push_back(bytes[3]);
                
                } else if (op->type == BAL::Operation::SS) {

                    uint8_t bytes[6] = { op->opcode, 0, 0, 0, 0, 0 };
                    uint16_t temp;
                    
                    // R1
                    ss >> temp;
                    bytes[1] += temp << 4;
                    
                    // R2
                    ss >> temp;
                    bytes[1] += temp;
                    
                    // D1, B1
                    if (ss.get() && isdigit(ss.peek())) {
                        // D1
                        ss >> temp;
                        bytes[2] += temp >> 8;
                        bytes[3] += temp;

                        // B1
                        ss >> temp;
                        bytes[2] += temp << 4;
                    } else {
                        LabelInstance resolveMe;
                        ss >> resolveMe.label;
                        resolveMe.pos = bytecode.size() + 1;
                        labelsToResolve.push_back(resolveMe);
                    }
                    
                    // D2, B2
                    if (ss.get() && isdigit(ss.peek())) {
                        // D2
                        ss >> temp;
                        bytes[4] += temp >> 8;
                        bytes[5] += temp;

                        // B2
                        ss >> temp;
                        bytes[4] += temp << 4;
                    } else {
                        LabelInstance resolveMe;
                        ss >> resolveMe.label;
                        resolveMe.pos = bytecode.size() + 1;
                        labelsToResolve.push_back(resolveMe);
                    }
                    
                    
                    bytecode.push_back(bytes[0]);
                    bytecode.push_back(bytes[1]);
                    bytecode.push_back(bytes[2]);
                    bytecode.push_back(bytes[3]);
                    bytecode.push_back(bytes[4]);
                    bytecode.push_back(bytes[5]);

                } else {
                    std::cout << "### Operation type could not be determined\n";
                }
                
            } else if (mnemonic == "CSECT") {
            } else if (mnemonic == "USING") {
            } else if (mnemonic == "DS") {
                std::string params2 = params;
                std::replace_if(params2.begin(), params2.end(), ::ispunct, ' ');
                std::stringstream ss(params2);
            } else if (mnemonic == "DC") {
                std::string params2 = params;
                std::replace_if(params2.begin(), params2.end(), ::ispunct, ' ');
                std::stringstream ss(params2);
                
                std::string type;
                ss >> type;
                if (type == "F") {
                    uint32_t value;
                    ss >> value;
                    
                    bytecode.push_back(value >> 24);
                    bytecode.push_back(value >> 16);
                    bytecode.push_back(value >> 8);
                    bytecode.push_back(value);
                }
                
            } else if (mnemonic == "END") {
            } else if (mnemonic == "XDUMP") {
                bytecode.push_back(0xE0);
                bytecode.push_back(0x00);
                bytecode.push_back(0x00);
                bytecode.push_back(0x00);
                bytecode.push_back(0x00);
                bytecode.push_back(0x00);
            }
            
            std::cout << std::hex << std::right << std::setw(6) << std::setfill('0') << bytepos;
            std::cout << "   ";
            for (size_t i = 0; i < 12 - ((bytecode.size() - bytepos) * 2); ++i) std::cout << ' ';
            for (size_t i = bytepos; i < bytecode.size(); ++i) {
                std::cout << std::hex << std::right << std::setw(2) << std::setfill('0') << (int)bytecode[i];
            }
            std::cout << std::setfill(' ') << "   ";
            std::cout << std::setw(8) << std::left << label << " " << std::setw(5) << mnemonic << " " << params << '\n';
            
            bytepos = bytecode.size();
        }
        
        for (auto &it : labelsToResolve) {
            bytecode[it.pos + 1] += 0xF0;
            bytecode[it.pos + 1] += labels[it.label] >> 8;
            bytecode[it.pos + 2] += labels[it.label];
        }

        std::ofstream outfile(filename, std::ios::binary);
        if (!outfile) return false;
        outfile.write(reinterpret_cast<const char *>(bytecode.data()), bytecode.size());

        return true;
    }

private:

    BAL::Instructions instructions;

    std::vector<std::string> lines;
    std::map<std::string, uint32_t> labels;

    void ShowError(int line, std::string error) {
        std::cout << "## WARNING @ LINE (" << line << "): " << error << '\n';
    }
    
    void ParseLine(size_t i, std::string &label, std::string &op, std::string &params) {
            std::string line = lines.at(i - 1);
            uint8_t pos = 0;
            if (line.size() < 16) line.append(16 - lines.size(), ' ');
            
            label  = line.substr(0, 9);
            op     = line.substr(9, 6);
            params = line.substr(15,line.size() - 15);
            
            pos = label.find_first_of(' ');
            if (pos == 0) {
                if (label.find_first_not_of(' ') < label.size()) {
                    ShowError(i, "Label doesn't begin on col 0");
                }
                label = "";
            } else if (pos < label.size()) {
                if (label.find_first_not_of(' ', pos) != std::string::npos) {
                    ShowError(i, "Cannot have multiple labels");
                }
                if (isdigit(label[0])) {
                    ShowError(i, "Label cannot start with numeric character");
                }
                label = label.substr(0, pos);
            } else {
                ShowError(i, "Label is too long; passed col 8");
                label = label.substr(0, label.size() - 1);
            }
            
            pos = op.find_first_of(' ');
            if (pos == 0) {
                if (op.find_first_not_of(' ', pos) != std::string::npos) {
                    ShowError(i, "Operation doesn't begin on col 9");
                }
                op = "";
            } else if (pos < op.size()) {
                std::string test = op.substr(pos, op.size() - pos);
                if (test.find_first_not_of(' ') < test.size()) {
                    ShowError(i, "Cannot have multiple operations");
                }
                op = op.substr(0, pos);
            } else {
                ShowError(i, "Operation too long; passed col 14");
                op = op.substr(0, op.size() - 1);
            }
            
            pos = params.find_first_of(' ');
            if (pos > 0) {
                if (params.find('\'') != std::string::npos) {
                    pos = params.find_last_of('\'') + 1;
                }
                params = params.substr(0, pos);
            }
    }

};

