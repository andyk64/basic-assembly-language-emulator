#pragma once

// modded memcpy to reverse endian-ness
void memcpy2(void* dest, const void* src, std::size_t count) {
    char *csrc = (char *)src;
    char *cdest = (char *)dest;
    for (std::size_t i = 0; i < count; ++i) {
        cdest[i] = csrc[count - i - 1];
    }
}

uint8_t swapNibbles(uint8_t *src) {
    return (*src & 0xF0) >> 4 | (*src & 0x0F) << 4;
}

uint8_t high(uint8_t *src) {
    return (*src & 0xF0) >> 4;
}

uint8_t low(uint8_t *src) {
    return (*src & 0x0F);
}
