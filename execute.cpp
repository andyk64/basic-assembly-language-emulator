#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include "execute.h"

typedef uint8_t byte;

void print_error(int line_number, std::string error) {
    std::cout << "### WARNING LINE (" << line_number << "): " << error << '\n';
}

int main(int argc, char *argv[])
{
    struct Label {
        std::string label;
        uint32_t starting_byte;
        std::vector<uint32_t> references;
    };
    
    if (argc < 2) {
        std::cout << "Not enough arguments\n";
        return -1;
    }
    
    std::ifstream infile(argv[1], std::ios::binary);
    if (!infile) {
        std::cout << "Error opening input file\n";
        return -1;
    }

    infile.seekg(0, infile.end);
    BAL::program_size = infile.tellg();
    infile.seekg(0, infile.beg);

    std::vector<char> program;
    program.resize(BAL::program_size);
    infile.read(program.data(), BAL::program_size);
    BAL::program = reinterpret_cast<uint8_t *>(program.data());

    BAL::bytecode_to_function();
    BAL::Run();

    return 0;
}
