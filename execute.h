#pragma once

#include <iostream>
#include <vector>
#include <array>
#include <map>
#include <functional>
#include <cstring>
#include <iomanip>

#include "util.h"

namespace BAL {
    uint8_t *program;
    uint32_t program_size;
    uint32_t head;
    std::array<int32_t, 16> registers;

    std::map<std::string, uint8_t> text_opcodes;
    std::array<std::function<void()>, 0xFF> op;
    
    void memcpy2(void* dest, const void* src, std::size_t count) {
        char *csrc = (char *)src;
        char *cdest = (char *)dest;
        for (std::size_t i = 0; i < count; ++i) {
            cdest[i] = csrc[count - i - 1];
        }
    }
    
    void Run() {
        registers.fill(0xF5F5F5F5);
        registers[14] = program_size;
        registers[15] = 0;
        std::string x;
        
        for (size_t i = 0; i < program_size; ++i) {
            if (i % 8 == 0) std::cout << '\n';
            std::cout << std::hex << std::setfill('0') << std::right << std::setw(2) << (int)program[i];
        }
        std::cout << std::setfill(' ') << '\n';
        
        while (head <= program_size) {
            std::cout << std::hex << (int)program[head];
            op[program[head]]();
            std::getline(std::cin, x);
        }
    }

void instruction_to_bytecode() {
text_opcodes.insert(std::pair<std::string, uint8_t>("BR"   ,0x07));
text_opcodes.insert(std::pair<std::string, uint8_t>("L"    ,0x18));
text_opcodes.insert(std::pair<std::string, uint8_t>("AR"   ,0x1A));
text_opcodes.insert(std::pair<std::string, uint8_t>("SR"   ,0x1B));
text_opcodes.insert(std::pair<std::string, uint8_t>("ST"   ,0x50));
}

    namespace Operations {
    
        struct RR {
            uint16_t opcode : 8;
            uint16_t r1 : 4;
            uint16_t r2 : 4;
            
            RR(uint8_t buffer[]) {
                opcode = *buffer;
                r1 = high(buffer + 1);
                r2 = low(buffer + 1);
            }
        };
        
        struct RX {
	    uint16_t opcode : 8;
	    uint16_t r : 4;
	    uint16_t x : 4;
	    uint16_t b : 4;
	    uint16_t d : 12;
            
            RX(uint8_t buffer[]) {
                opcode = *buffer;
                r = high(buffer + 1);
                x = low(buffer + 1);
                b = high(buffer + 2);
                d = low(buffer + 2) + *(buffer + 3);
            }
        };
        
        struct RS {
            uint16_t opcode : 8;
            uint16_t r1 : 4;
            uint16_t r2 : 4;
            uint16_t b : 4;
            uint16_t d : 12;
            
            RS(uint8_t buffer[]) {
                opcode = *buffer;
                r1 = high(buffer + 1);
                r2 = low(buffer + 1);
                b = high(buffer + 2);
                d += low(buffer + 2) + *(buffer + 3);
            }
        };
        
        struct SI {
            uint16_t opcode : 8;
            uint16_t l : 8;
            uint16_t b : 4;
            uint16_t d : 12;
        };
        
        struct SS {
            uint16_t opcode : 8;
            uint16_t L : 8;
            uint16_t L1 : 4;
        };

        // placeholder instruction
        void NA() {
            std::cout << "NOP\n";
        }
    
        void BR() {
            RR params(&program[head]);
            
            head += registers[params.r2];
        }
        
        void L() {
            RX params(&program[head]);
            
            uint32_t index = 0;
            if (params.x != 0) index = registers[params.x];
            uint32_t base = 0;
            if (params.b != 0) base = registers[params.b];
            uint32_t addr = index + base + params.d;
            
            memcpy2(&registers[params.r], &program[addr], sizeof(uint32_t));
            head += sizeof(params);
        }
        
        void AR() {
            RR params(&program[head]);
            
            registers[params.r1] += registers[params.r2];
            head += sizeof(params);
        }
        
        void SR() {
            RR params(&program[head]);

            registers[params.r1] -= registers[params.r2];
            head += sizeof(params);
        }
        
        void ST() {
        }
        
        void XDUMP() {
            std::cout << '\n' << std::left << std::setw(12) << "REGS 0-7";
            for (int i = 0; i < 8; i++) {
                std::cout << std::right << std::setw(8) << std::setfill('0') << std::hex << registers[i] << "    ";
            }

            std::cout << '\n' << std::left << std::setw(12) << std::setfill(' ') << "REGS 8-15";
            for (int i = 8; i < 16; i++) {
                std::cout << std::right << std::setw(8) << std::setfill('0') << std::hex << registers[i] << "    ";
            }
            std::setfill(' ');
            
            head += 6;
        }
    }


void bytecode_to_function() {
    op.fill(BAL::Operations::NA);
    op[0x07] = BAL::Operations::BR;
    op[0x1A] = BAL::Operations::AR;
    op[0x1B] = BAL::Operations::SR;
    op[0x58] = BAL::Operations::L;
    op[0xE0] = BAL::Operations::XDUMP;
}

}

